libsys-gamin-perl (0.1-3) unstable; urgency=medium

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.
  * Remove Andres Mejia from Uploaders on request of the MIA team.
    (Closes: #743559)

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/control: update Build-Depends for cross builds.
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Bump debhelper from old 12 to 13.
  * Set Testsuite header for perl package.

  [ Étienne Mollier ]
  * d/rules: enable hardening.
  * d/control: declare myself as uploader.
  * d/control: declare conformance to standards version 4.6.1.
  * d/control: Rules-Requires-Root: no.

 -- Étienne Mollier <emollier@debian.org>  Tue, 24 May 2022 20:51:05 +0200

libsys-gamin-perl (0.1-2) unstable; urgency=low

  * Team upload.

  [ Andres Mejia ]
  * Remove comment about bug #510368 in debian/rules as it's not a bug.

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ gregor herrmann ]
  * debian/control: un-wrap Vcs-Browser field.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Switch to "3.0 (quilt)" source format.
  * debian/rules: use short-form dh(1) version.
  * Drop remaining quilt fragments.
  * debian/copyright: refresh license stanzas.
  * Use debhelper 9.20120312 to get all hardening flags.
  * Declare compliance with Debian Policy 3.9.5.

 -- gregor herrmann <gregoa@debian.org>  Sat, 22 Feb 2014 18:36:02 +0100

libsys-gamin-perl (0.1-1) unstable; urgency=low

  * Initial release. (Closes: #464891)

 -- Andres Mejia <mcitadel@gmail.com>  Wed, 31 Dec 2008 15:18:19 -0500
